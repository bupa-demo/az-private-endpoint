output "private_endpoint_name" {
  value = azurerm_private_endpoint.private_endpoint.name
}

output "private_endpoint_id" {
  value = azurerm_private_endpoint.private_endpoint.id
}